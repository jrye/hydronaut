`Hydronaut <https://gitlab.inria.fr/jrye/hydronaut>`_ Documentation
===================================================================

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    readme
    modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
