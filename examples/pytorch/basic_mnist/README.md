---
title: PyTorch Basic MNIST Example README
---

# Synopsis

This is an example of how to adapt [PyTorch's Basic MNIST Example](https://github.com/pytorch/examples/tree/main/mnist) to Hydronaut. It is intentionally simple to show how existing code can be integrated into the framework with minimal changes.

This example was produced by the online tutorial [here](https://jrye.gitlabpages.inria.fr/hydronaut-tutorial/notebooks/02-PyTorch-Basic_MNIST_Example.slides.html). Please see the slides and corresponding Jupyter notebook for details.

# Usage

~~~sh
hydronaut-run [--multirun]
~~~
