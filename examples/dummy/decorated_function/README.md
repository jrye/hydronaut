---
title: Dummy Decorated Function Example README
---

# Synopsis

A variation of the [dummy Experiment subclass example](../experiment_subclass) that illustrates how to run [a user function](decorated_function.py) with Hydronaut via a Python decorator.

# Usage

~~~sh
python3 ./decorated_function.py
~~~
