---
title: Dummy Decorated Class Example README
---

# Synopsis

A variation of the [dummy Experiment subclass example](../experiment_subclass) that illustrates how to run [a user class](decorated_class.py) with Hydronaut via a Python decorator.

# Usage

~~~sh
python3 ./decorated_class.py
~~~
