---
title: Dummy Examples README
---

# Synopsis

This directory contains dummy examples which simply recover 3 values from the configuration file and return their product.
