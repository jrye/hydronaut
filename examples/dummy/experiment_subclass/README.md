---
title: Dummy Experiment Subclass Example README
---

# Synopsis

This example show how to create a [minimal subclass of the Experiment class](src/experiment.py) and return a single value calculated from parameters in the [configuration file](conf/config.yaml).

# Usage

~~~sh
hydronaut-run --multirun
~~~
